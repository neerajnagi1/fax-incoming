require 'rubygems'
require_relative 'multipart.rb'
require_relative 'actionmailer.rb'
require 'mongo'
require 'uri'
require 'net/http'
require 'sidekiq'
require 'json'
require 'redis'

class PostFaxData
include Sidekiq::Worker
include Multipart
include Mongo
client = MongoClient.new # defaults to localhost:27017
db     = client['fax']
coll   = db['settings']
@alerts = coll.find_one({name:"alerts"})
if(@alerts.nil?)
	@alerts=[]
else
	@alerts= @alerts['all'] 
end

def send_email(to,f_i,t_i)
        email = Mailer.fax(to,f_i,t_i)
        email.deliver

end

def turn_for_email(count)
if(count.nil?)
	return nil
else
        _window = 0
        _count = 0
client = MongoClient.new # defaults to localhost:27017
db     = client['fax']
coll   = db['settings']
alerts_coll = coll.find_one({name:"alerts"})
if(!alerts_coll.nil?)
	alerts = alerts_coll['all']
end
if(!alerts.nil?)
client.close()
        alerts.each do |alert|
			if !alert['email'].nil?
                                _count = _count +1
                                if count <= _count
                                        return alert['email']
                                end

	                elsif alert['span'].nil?
        	                _count = _count + 1
                	        if count <= _count
                        	        return false
	                        end

                        elsif alert['unit']=='day'
                                for i in 1..(1440*alert['span'].to_i/alert['minutes'].to_i)
                                        _count = _count + 1
                                        if count <= _count
                                                return false
                                        end

                                end


                        elsif alert['unit']=='week'
                                for i in 1..(7*1440*alert['span'].to_i/alert['minutes'].to_i)
                                        _count = _count + 1
                                        if count <= _count
                                                return false
                                        end

                                end

                        elsif alert['unit']=='month'
                                for i in 1..(30*1440*alert['span'].to_i/alert['minutes'].to_i)
                                        _count = _count + 1
                                        if count <= _count
                                                return false
                                        end

                                end

                        end
                end
	end
        return false
  end
end


def self.retry_count(count)
        _window = 0
        _count = 0
        @alerts.each do |alert|
                if alert['span'].nil?
                        _count = _count + 1
                        if count <= _count
                                return alert['minutes'].to_i*60
                        end

                else
                        if alert['unit']=='day'
                                for i in 1..(1440*alert['span'].to_i/alert['minutes'].to_i)
                                        _count = _count + 1
                                        if count <= _count
                                                return alert['minutes'].to_i*60
                                        end

                                end


                        elsif alert['unit']=='week'
                                for i in 1..(7*1440*alert['span'].to_i/alert['minutes'].to_i)
                                        _count = _count + 1
                                        if count <= _count
                                                return alert['minutes'].to_i*60
                                        end

                                end

                        elsif alert['unit']=='month'
                                for i in 1..(30*1440*alert['span'].to_i/alert['minutes'].to_i)
                                        _count = _count + 1
                                        if count <= _count
                                                return alert['minutes'].to_i*60
                                        end

                                end

                        end
                end
        end
        return _count-1
  end

sidekiq_options :retry => retry_count(600000).to_i, :dead =>true

  sidekiq_retries_exhausted do |msg|
	  Sidekiq.logger.warn "retry exhausted for #{msg}"
  end

  sidekiq_retry_in do |count|
	retry_count(count)
  end


        def perform(metadata)
          d =  JSON.parse(URI.unescape(metadata))
	r = Redis.new
	r.incr(d['uuid'])
	puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>  #{d}   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
_url =  d['result']['protocol'] + "://" + d['result']['url']
_url_1 = d['result_1']['protocol'] + "://" + d['result_1']['url']
_ssl_excp = d['result']['ssl_excp'].gsub(/[, ]+/,'_').split('_')
_ssl_excp_1 = d['result']['ssl_excp'].gsub(/[, ]+/,'_').split('_')
tags = _url.scan(/\{[^{}]*\}/)
tags.each do |tag|
	if(tag.downcase.gsub(/\{+|\}+|\s+/,'')=='duration')
        _url = _url.gsub(tag, (d[tag.downcase.gsub(/\{+|\}+|\s+/,'') ]/1000000).to_i.to_s)
	else
	        _url = _url.gsub(tag, d[tag.downcase.gsub(/\{+|\}+|\s+/,'') ].to_s)
	end
end

tags = _url_1.scan(/\{[^{}]*\}/)
tags.each do |tag|
	        if(tag.downcase.gsub(/\{+|\}+|\s+/,'')=='duration')
        _url_1 = _url_1.gsub(tag, (d[tag.downcase.gsub(/\{+|\}+|\s+/,'') ]/1000000).to_i.to_s)
        else
                _url_1 = _url_1.gsub(tag, d[tag.downcase.gsub(/\{+|\}+|\s+/,'') ].to_s)
        end

end

tags_1 = _url_1.match(/\?.*$/).to_s
tags =_url.match(/\?.*$/).to_s




url = URI.parse(_url)
url_1 = URI.parse(_url_1)
if(url.path == "")
_url = _url+"/"
end
if(url_1.path == "")
_url_1 = _url_1 + "/"
end
puts _url
puts d
params = {uuid: d['uuid'] ,
e164: d['e164'] ,
cli: d['cli'] ,
utc: d['utc'] ,
speed: d['speed'] ,
ecm: d['ecm'] ,
compression:  d['compression'] ,
duration: (d['duration']/1000000).to_i.to_s ,
pages: d['pages'] ,
rsid: d['rsid'] ,
rdnis: d['rdnis']

}
file = File.open("/var/data/fax/in/#{d['uuid']}.tif", "rb")
 
params["file"] = file


# TODO: need auth
mp = Multipart::MultipartPost.new
# Get both the headers and the query ready, given the new MultipartPost and the params Hash
query, headers = mp.prepare_query(params)
 
file.close
require 'net/http'
def self.post_form(url, query, headers, tags, ssl_excp)


#http = Net::HTTP.new(url.host, url.port)
if url.scheme=='https'
	use_ssl = true
else
	use_ssl = false
end


if(use_ssl)

	
	v_res = `echo -e "quit\n" | openssl s_client -connect #{url.host}:#{url.port} -tlsextdebug 2>&1`.to_s
	v_res = v_res.scan(/verify error:num=\d+/).map{|err| err.gsub('verify error:num=','').to_i}

	exclude_error = []
	if( !ssl_excp.nil?)
		exclude_error = ssl_excp.map {|n| n.to_i}
	end
	Sidekiq.logger.warn "excluding errors #{exclude_error}"
	v_res = v_res - exclude_error
	if(v_res.length != 0)
		Sidekiq.logger.warn "ssl verification has pending errors #{v_res}"
		puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx pending error"
		return
	end
end

        puts  "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxurl-> #{url}   path -> #{url.path + tags} header-> #{headers}"

 Net::HTTP.start(url.host, url.port, :use_ssl=> use_ssl,  :verify_mode => OpenSSL::SSL::VERIFY_NONE) {|con|
    con.read_timeout =300 
    begin
	Sidekiq.logger.warn "path -> #{url.path + tags} header-> #{headers}"
      return con.post(url.path + tags, query, headers)
    rescue => e
      puts "POSTING Failed #{e}... #{Time.now}"
    end
  }
end


url = URI.parse(_url)
url_1 = URI.parse(_url_1)

email = turn_for_email(r.get(d['uuid']).to_i)
Sidekiq.logger.warn "turn for email check #{email}  #{r.get(d['uuid']).to_i}"
if(email)
	Sidekiq.logger.warn "sending mail to #{d['result']['email']} #{d['result_1']['email']} url is #{_url}"
#send email notification
	externalip = `curl http://169.254.169.254/latest/meta-data/public-ipv4`
	send_email(d['result']['email'],externalip,url.host)
        send_email(d['result_1']['email'],externalip,url_1.host)
else
#post form data
Sidekiq.logger.warn "posting to ##1 #{url}"
begin
response = post_form(url, query, headers,tags,_ssl_excp)
rescue Exception=>e
puts e
end
Sidekiq.logger.warn "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#{response}"
if !(response.kind_of? Net::HTTPSuccess)
	Sidekiq.logger.warn "posting to ##2 #{url_1}"

	response = post_form(url_1, query, headers,tags_1, _ssl_excp_1)
end 
case response
when Net::HTTPSuccess
  puts "Hooray, got response: #{response.inspect}"
  File.delete(file.path)
when Net::HTTPInternalServerError
  raise "Server blew up"
else
  raise "Unknown error: #{response}"
end
end

end
end
