require 'sidekiq'
require_relative 'aws.rb'
class ProcessIncomingFax

include Sidekiq::Worker

	def perform(metadata)
		puts "new job recieved #{metadata}"
		aws = Aws.new
		aws.push_recording("#{metadata["uuid"]}.tif" , metadata["attachment"])
	end

end
