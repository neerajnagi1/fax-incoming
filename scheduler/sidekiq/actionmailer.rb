require 'action_mailer'

ActionMailer::Base.raise_delivery_errors = true
ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
   :address   => "172.16.0.127",
   :port      => 25,
   :domain    => "id.comunycarse.com",
   :authentication => :login,
   :user_name      => "neeraj",
   :password       => "neeraj",
   :enable_starttls_auto => true
  }
ActionMailer::Base.view_paths= File.dirname(__FILE__)

class Mailer < ActionMailer::Base
  def fax(to,from_ip, to_ip)
    @from_ip = from_ip
	@to_ip = to_ip

    mail(   :to      => to,#"javier@id.comunycarse.com",
            :from    => "neeraj@id.comunycarse.com",
            :subject => "Fax notification retry limit reached") do |format|
                format.html
		format.text
    end
  end
end

