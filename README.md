#Installation Steps


#GIT
##copy repo
```
cd /usr/src
git clone https://neerajnagi1@bitbucket.org/neerajnagi1/fax-incoming.git
```

#freeswitch

##Install dependencies and download source
```

cd /usr/src

wget http://downloads.xiph.org/releases/speex/speex-1.2rc1.tar.gz
tar -zxvf speex-1.2rc1.tar.gz
cd speex-1.2rc1
./configure --enable-shared --prefix=/usr/local/lib/speex
make && make install
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/speex/lib/pkgconfig/


sudo apt-get install git-core subversion build-essential autoconf automake libtool libncurses5 libncurses5-dev make libjpeg-dev libldns-dev libedit-dev
sudo apt-get install libcurl4-openssl-dev libexpat1-dev libgnutls-dev libtiff4-dev libx11-dev unixodbc-dev libssl-dev python2.6-dev \
                       zlib1g-dev libzrtpcpp-dev libasound2-dev libogg-dev libvorbis-dev libperl-dev libgdbm-dev libdb-dev python-dev \
                       uuid-dev sqlite3 libsqlite3-dev libpcre3 libpcre3-dev libspeex1 libspeex-dev libspeexdsp1 libspeexdsp-dev
```

##download freeswitch git
```
git clone  https://stash.freeswitch.org/scm/fs/freeswitch.git
 
cd freeswitch
```
##compile and install
```
./bootstrap.sh
./configure
make
sudo make install
```
##add freeswitch executable in path
```
 export PATH=$PATH:/usr/local/freeswitch/bin
```


## copy conf from this git
```
cp -R /usr/src/fax-incoming/freeswitch/conf /usr/local/freeswitch
cp /usr/src/fax-incoming/freeswitch/scripts/* /usr/local/freeswitch/scripts/
mkdir /var/data/fax/in
chmod -R 777 /var/data/fax/in
```
##Start freeswitch
```
nohup freeswitch -nc & 
```
## test its startup
```
fs_cli
```


#redis

##Install from apt
```
apt-get install redis-server
```

#ruby 
##Install from rvm
```
\curl -L https://get.rvm.io |    bash -s stable --ruby --autolibs=enable --auto-dotfiles
source /usr/local/rvm/scripts/rvm ( exact path in message in console )
rvm use ruby
```

#npm
```
aptitude install npm
ln -s /usr/bin/nodejs /usr/bin/node
```

#mongodb
```
apt-get install mongodb-server
chmod -R 777 /var/lib/mongodb
start mongodb
```
#Admin Dashboard 
```
export MONGO_URL=mongodb://localhost:27017/fax
npm install -g meteorite
cd /usr/src/fax-incoming/admin/dashboard
mrt update
curl https://install.meteor.com | /bin/sh
## start meteor  and go to  localhost:9000 or xxx.xxx.xxx.xxx:9000
nohup  meteor -p 9000 &
## check
go to IP:9090
```

#Event listener startup
```
cd  /usr/src/fax-incoming/server 
npm install
nohup node server.js &
```

#Task scheduler

## setup
```
cp -R /usr/src/fax-incoming/scheduler
rm Gemfile.lock
bundle install
```
## startup
```
nohup sidekiq -r ./worker.rb &
```
## smtp setup
```
sudo apt-get install postfix

## add mongodb and redis in chkconfig
```
chkconfig mongod on
chkconfig redis on
```



##CDR
###enable mod-cdr-sqlite
```
cd /usr/src/freeswitch
make mod_cdr_sqlite-install
fs_cli
load mod_cdr_sqlite 
```
###henceforth all cdrs will be available in /usr/local/freeswitch/db/cdr.db, which is a sqlite database file
```
cd /usr/local/freeswitch/db
sqlite3 cdr.db
>select * from cdr;
```
###add a new template in mod_cdr_sqlite 
```
<param name="default-template" value="fax"/>
    <!-- This is like the info app but after the call is hung up -->
    <!--<param name="debug" value="true"/>-->
  </settings>
  <templates>
    <!-- Note that field order must match SQL table schema, otherwise insert will fail -->
    <template name="fax">"${caller_id_name}","${caller_id_number}","${destination_number}","${context}","${start_stamp}","${answer_stamp}","${end_stamp}",${duration},${billsec},"${hangup_cause}","${uuid}","${bleg_uuid}","${accountcode}", "${fax_result_code}", "${fax_result_text}", "${fax_document_transferred_pages}", "${fax_document_total_pages}", "${fax_image_resolution}", "${fax_image_size}", "${fax_bad_rows}", "${fax_transfer_rate}", "${fax_ecm_used}", "${fax_local_station_id}", "${fax_remote_station_id}"</template>
```
###modify sqlite schema for table 'cdr'
```
sqlite3 /usr/local/freeswitch/db/cdr.db
sqlite> ALTER TABLE cdr ADD COLUMN fax_result_code VARCHAR;
ALTER TABLE cdr ADD COLUMN fax_result_text VARCHAR;
ALTER TABLE cdr ADD COLUMN fax_document_transferred_pages VARCHAR;
ALTER TABLE cdr ADD COLUMN fax_document_total_pages VARCHAR;
ALTER TABLE cdr ADD COLUMN fax_image_resolution VARCHAR;
ALTER TABLE cdr ADD COLUMN fax_image_size VARCHAR;
ALTER TABLE cdr ADD COLUMN fax_bad_rows VARCHAR;
ALTER TABLE cdr ADD COLUMN fax_transfer_rate VARCHAR;
ALTER TABLE cdr ADD COLUMN fax_ecm_used VARCHAR;
ALTER TABLE cdr ADD COLUMN fax_local_station_id VARCHAR;
ALTER TABLE cdr ADD COLUMN fax_remote_station_id VARCHAR;
```
Reloading of mod_cdr_sqlite from fs_cli:
reload mod_cdr_sqlite

