// Set up a collection to contain url information. On the server,
// it is backed by a MongoDB collection named "urls".

Settings= new Meteor.Collection("settings");
Urls = new Meteor.Collection("urls");
Dids = new Meteor.Collection("dids");
CapacityGroups = new Meteor.Collection("c_groups");
if (Meteor.isClient) {


  Template.admin.urls = function () {
    return Urls.find({}, {sort: {created_at:-1}});
  };
  Template.admin.settings = function(){
	return Settings.findOne({name:"alerts"}, {sort:{created_at:-1}});
}
  Template.admin.dids = function(){
	return Dids.find({}, {sort: {created_at:-1}});
}
	Template.admin.c_groups = function(){
	return CapacityGroups.find({}, {sort:{created_at:-1}});
}
	Template.admin.rendered = function(){


		  AutoCompletion.init("input#searchBoxDid");
		  AutoCompletion.init("input#searchBoxUrl");


		$("#tabs").tabs();
		$("#b_detail_did").click(function(){
			var res = Dids.findOne({v_number:$("#searchBoxDid").val()  });	
		         var url = Urls.findOne({_id:res.url_id});
                if(url)
                        var did_url = url.verb+" "+url.protocol+" "+url.url;
                var c_group = CapacityGroups.findOne({_id:res.c_group_id})
                if(c_group)
                        var did_cgroup =  c_group.name+"/"+c_group.limit;




			$("#didSearchResult").html("<hr>Search Result::<span style='color:GREEN'>"+res.v_number+"  "+did_url+"  "+did_cgroup+"</span><input type='submit' value ='edit' id='b_edit_search_did' >&nbsp;&nbsp;&nbsp;<input type='submit' value ='delete' id='b_delete_search_did' ><hr>");
			                $("#b_edit_search_did").click(function(){
			                        $("#v_number").val(res.v_number);
                        $("#s_url").val(res.url_id);
                        $("#c_group").val(res.c_group_id);
                        $("#did_id").val(res._id);
				});

					$("#b_delete_search_did").click(function(){
				 Dids.remove({_id:res._id});
			});


	});

    $("#b_detail_url").click(function(){
                        var res = Urls.findOne({url:$("#searchBoxUrl").val()  });

                        $("#urlSearchResult").html("<hr>Search Result::<span style='color:GREEN'>"+res.verb+"  "+res.protocol+"  "+res.url+" "+res.csid+" "+res.description+"</span><input type='submit' value ='edit' id='b_edit_search_url' >&nbsp;&nbsp;&nbsp;<input type='submit' value ='delete' id='b_delete_search_url' ><hr>");

		$("#b_edit_search_url").click(function(){
                               var res = Urls.findOne({url:$("#searchBoxUrl").val()  });

                         $("#verb").val(res.verb)
         $("#protocol").val(res.protocol);
         $("#url").val(res.url);
        $("#csid").val(res.csid);
         $("#description").val(res.description);
        $("#url_id").val(res._id);
        $("#email").val(res.email);

        $("#ssl_excp").val(res.ssl_excp);
        e.preventDefault();
                });

                         $("#b_delete_search_url").click(function(){
                                 Urls.remove({_id:res._id});
                        });


        });



		$("#b_add_url").click(function(){
			$.post(
				"/add_url",
				{id: $("#url_id").val() , verb:$("#verb").val(), protocol:$("#protocol").val(), url:$("#url").val(),csid:$("#csid").val(), description:$("#description").val() , email:$("#email").val() ,  ssl_excp:$("#ssl_excp").val()
}

			);
			$("#url_id").val("");
		});
		$("#b_add_did").click(function(){
	
			$.post(
                                "/add_did",
                                {id:$("#did_id").val() , v_number:$("#v_number").val(), url:$("#s_url").val() , url_1:$("#s_url_1").val(), c_group:$("#c_group").val() }
                        ); 
			$("#did_id").val("");
		});
                $("#b_add_c_group").click(function(){

                        $.post(
                                "/add_c_group",
                                {id:$("#c_group_id").val(), name:$("#c_group_name").val(), limit:$("#limit").val()  }
                        ); 
			$("#c_group_id").val("");
		});

		$("#b_add_setting").click(function(){
			$.post( 
				"/add_setting",
				{minutes:$("#minutes").val() }
			);
		});
                $("#b_add_setting_1").click(function(){
                        $.post(
                                "/add_setting_1",
                                {minutes:$("#minutes_for").val(), span:$("#for_quantity").val(), unit:$("#quantity_unit").val()  }
                        );
                });
                $("#b_add_setting_2").click(function(){
                        $.post(
                                "/add_setting_2",
                                {email:$("#email_to").val()  }
                        );
                });

		$("#alerts_delete").click(function(){
			$.post(
				"/delete_alerts"
			);
		});
		$("#dialog").dialog({ minHeight: 600 , minWidth:800});
	$("#dialog").dialog("close");
		$("#ssl_err_detail").click(function(){
			$("#dialog").dialog("open");
		});


}

  Template.admin.events({
    'click input.inc': function () {
      Urls.update(Session.get("selected_url"), {$inc: {score: 5}});}
  
,	
  'keyup input#searchBoxDid': function () {
    AutoCompletion.autocomplete({
      element: 'input#searchBoxDid',       // DOM identifier for the element
      collection: Dids,              // MeteorJS collection object
      field: 'v_number',                    // Document field name to search for
      limit: 0,                         // Max number of elements to show
      sort: { name: 1 }});              // Sort object to filter results with
      //filter: { 'gender': 'female' }}); // Additional filtering
  }
,
   
  'keyup input#searchBoxUrl': function () {
    AutoCompletion.autocomplete({
      element: 'input#searchBoxUrl',       // DOM identifier for the element
      collection: Urls,              // MeteorJS collection object
      field: 'url',                    // Document field name to search for
      limit: 0,                         // Max number of elements to show
      sort: { name: 1 }});              // Sort object to filter results with
      //filter: { 'gender': 'female' }}); // Additional filtering
  }


  
  });

  Template.url.events({
    'click': function () {
      Session.set("selected_url", this._id);
    }
,
   'click button.delete': function(){ Urls.remove({_id:this._id});}
,
  'click button.edit' : function(){ $("#verb").val(this.verb)
	 $("#protocol").val(this.protocol);
	 $("#url").val(this.url); 
	$("#csid").val(this.csid);
	 $("#description").val(this.description);   
	$("#url_id").val(this._id);
	$("#email").val(this.email);

	$("#ssl_excp").val(this.ssl_excp);
}

  });

	Template.did.events(
	{
		'click button.delete': function(){ Dids.remove({ _id:this._id });  } , 
			'click button.edit': function(){
			$("#v_number").val(this.v_number);
			$("#s_url").val(this.url_id);
			$("#c_group").val(this.c_group_id);
			$("#did_id").val(this._id);
		}

	}
	);

  Template.c_group.events(
        {
                'click button.delete': function(){ CapacityGroups.remove({ _id:this._id });  } ,
                        'click button.edit': function(){
                        $("#c_group_name").val(this.name);
                        $("#limit").val(this.limit);
			$("#c_group_id").val(this._id);
                }

        }
        );

	Handlebars.registerHelper("url_from_id", function(id) {

		var url = Urls.findOne({_id:id});
		console.log("in handlebar "+ id);
		if(url)
			return url.verb+" "+url.protocol+" "+url.url;
});
        Handlebars.registerHelper("c_group_from_id", function(id) {
		var c_group = CapacityGroups.findOne({_id:id})
		if(c_group)
			return c_group.name+"/"+c_group.limit;
});


}

// On server startup, create some urls if the database is empty.
if (Meteor.isServer) {
  Meteor.startup(function () {

Meteor.Router.add('/add_url', 'POST', function(id) {
  // update Item Function
	console.log("hi");
	data = this.request.body
	console.log(data)
	Urls.update({ _id:data.id }, { created_at:(new Date()).getTime(), verb:data.verb , protocol:data.protocol, url:data.url, csid:data.csid, description:data.description, email:data.email , ssl_excp:data.ssl_excp }, {upsert:true} );

	return  ;
});

Meteor.Router.add('/add_did', 'POST', function(id){
	data = this.request.body;
	console.log(data);
	Dids.update({_id:data.id}, { created_at:(new Date()).getTime() , v_number:data.v_number, url_id:data.url,url_id_1:data.url_1, c_group_id:data.c_group}, {upsert:true});
});

Meteor.Router.add('/add_c_group', 'POST', function(id){
        data = this.request.body;
	console.log(data);
        CapacityGroups.update({_id:data.id}, { created_at:(new Date()).getTime() , name:data.name, limit:data.limit}, {upsert:true});
})

Meteor.Router.add('/add_setting_2', 'POST', function(id){
        data = this.request.body;
        Settings.update({name:"alerts"},{$push:{all: { created_at:(new Date()).getTime() , email:data.email  }}} , {upsert:true});
});


Meteor.Router.add('/add_setting_1', 'POST', function(id){
	data = this.request.body;
	Settings.update({name:"alerts"},{$push:{all: { created_at:(new Date()).getTime() , minutes:data.minutes , span:data.span, unit:data.unit  }}} , {upsert:true});
});
Meteor.Router.add('/add_setting', 'POST', function(id){
        data = this.request.body;
        Settings.update({name:"alerts"},{$push:{all: { created_at:(new Date()).getTime() , minutes:data.minutes }}} , {upsert:true});
});
Meteor.Router.add("/delete_alerts", 'POST', function(){ Settings.update({name:"alerts"},{all:[]} );  });

  });
}
